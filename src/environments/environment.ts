// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB7JjAv0PRjgeX1QrDVvF9mDzwyLd2ezks",
    authDomain: "bbbc-70bcd.firebaseapp.com",
    databaseURL: "https://bbbc-70bcd.firebaseio.com",
    projectId: "bbbc-70bcd",
    storageBucket: "bbbc-70bcd.appspot.com",
    messagingSenderId: "419794428661",
    appId: "1:419794428661:web:1cb2e29410aec049545bbc",
    measurementId: "G-WX1N1Y41TD"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
