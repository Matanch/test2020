import { WeatherRaw } from './interfaces/weather-raw';
import { Weather } from './interfaces/weather';
import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse} from '@angular/common/http';
import { Observable , throwError} from 'rxjs';
import { map , catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TempService {

  private URL ="http://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "9ac24013979be036a921753007aa5d57";
  private IMP = "&units=metric";

  searchWeatherData(cityName:String):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(
      map(data => this.transfromWeatherData(data)),
      catchError(this.handleError)
      )
  }

  private handleError(res:HttpErrorResponse){
    
    console.log("in the servic" + res.error)
    return throwError(res.error)
  }

  private transfromWeatherData(data:WeatherRaw):Weather{
    return {
      name:data.name,
      country:data.sys.country,
      image: `https://api.openweathermap.org/img/w/${data.weather[0].icon}`,
      description: data.weather[0].description,
      temperature: data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
  }
  constructor(private http:HttpClient) { }
}
